#!/bin/sh

getent group polkitd >/dev/null || groupadd -r polkitd

getent passwd polkitd >/dev/null || \
useradd -r -M -g polkitd -G polkitd -c "PolicyKit Daemon Owner" -d /etc/polkit-1 -s /sbin/nologin polkitd

