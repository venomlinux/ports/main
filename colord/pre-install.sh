#!/bin/sh

getent group colord 2>/dev/null || groupadd -g 71 colord
getent passwd colord 2>/dev/null || \
useradd -r -M -g colord -G colord -c "Color Daemon Owner" -d /var/lib/colord -s /sbin/nologin -u 71 colord
