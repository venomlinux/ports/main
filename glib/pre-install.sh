#!/bin/sh

checkgiv() {

giv=$(scratch info gobject-introspection | awk '/Installed/{print $2}' | awk -F "-" '{print $1}')

[ "$giv" = "1.78.1" ] && scratch remove -y gobject-introspection

}

scratch isinstalled gobject-introspection && checkgiv
