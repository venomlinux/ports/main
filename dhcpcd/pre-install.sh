#!/bin/sh

getent group dhcpcd >/dev/null || groupadd -r dhcpcd
getent passwd dhcpcd >/dev/null || \
useradd -r -M -g dhcpcd -G dhcpcd -c "dhcpcd user" -d /var/lib/dhcpcd -s /sbin/nologin dhcpcd
