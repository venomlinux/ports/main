#!/bin/sh

kernver=$(find /lib/modules -maxdepth 1 -type d -iname '*-linux-libre' -printf "%f\n" )

# removing other venom's kernel
for i in /lib/modules/*; do
	[ -d $i ] || continue
	case ${i##*/} in
		$kernver) continue;;
		*-linux-libre|*-Venom-GNU)
			[ -d $i/build/include ] && continue
			echo "post-install: removing kernel ${i##*/}"
			rm -fr $i;;
	esac
done

if [ $(command -v mkinitramfs) ]; then
	echo "mkinitramfs: generating initramfs for kernel $kernver..."
	mkinitramfs -q -k $kernver -o /boot/initrd-linux-libre.img
fi

rm -f /boot/config-venom-gnu /boot/vmlinuz-venom-gnu /boot/initrd-venom-gnu.img

# load kernel modules
depmod $kernver

# reconfigure grub
if [ $(command -v grub-mkconfig) ]; then
	echo "grub-mkconfig: reconfigure for kernel $kernver..."
	grub-mkconfig -o /boot/grub/grub.cfg
fi 


